import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { CartComponent } from './cart/cart.component';
import { CategoryComponent } from './category/category.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ContactComponent } from './contact/contact.component';
import { ElementComponent } from './element/element.component';
import { HomeComponent } from './home/home.component';
import { SingleBlogComponent } from './single-blog/single-blog.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { TrackingComponent } from './tracking/tracking.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'category', component:CategoryComponent},
  {path:'blog', component:BlogComponent},
  {path:'contact', component:ContactComponent},
  {path:'element', component:ElementComponent},
  {path:'tracking', component:TrackingComponent},
  {path:'single-blog', component:SingleBlogComponent},
  {path:'single-product', component:SingleProductComponent},
  {path:'checkout', component:CheckoutComponent},
  {path:'cart', component:CartComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
